## D11 Clan Maps

Some classic Liero maps recovered from D11 Liero Clan website.

maps **Italy**, **Manjak**, **Super Battle**, **City** and **Fields** created by Briansrifle.

map **Military Base** created by Farnoy.

map **Arena** created by dan.

map **Hell** created by Aldrenean.

map **Inderdaad Arena** created by MrEvil.