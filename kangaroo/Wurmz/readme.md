## WURMZ! MAPS

### ttk

A map ported from another Liero clone - Wurmz!, with some adjustments made by myself and changed colors. Created in gimp. Original map made by kroLm & reworked by Bart.

### staticx

A map ported from another Liero clone - Wurmz!, with some adjustments made by myself and changed colors. Created in gimp. Original map made by Jaras & reworked by Greybrow.

### Design

A map ported from another Liero clone - Wurmz!, with some adjustments made by myself and changed colors. Created in gimp. Original map made by Leon Buikstra.
