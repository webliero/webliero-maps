## GUSANOS MAPS

### poo arena

A map ported from another Liero clone - gusanos, with some adjustments made by myself and changed colors. It is suitable for duels, but can be also played in ffa. Created in gimp. Original map made by Basara.

### poo_arena_extended

Same map as "poo arena", but changed a bit to adjust it to extended size. Created in gimp.

### blat2

Another map ported from gusanos, with some adjustments made by myself and changed colors. It is suitable for duels, but can be also played in ffa. Created in gimp. Original map made by Basara.

### wtf2

Another map ported from gusanos, with some adjustments made by myself and changed colors. It is suitable for duels, but can be also played in ffa. Created in gimp.

### wtf2_extended

Same map as "wtf2", but changed a bit to adjust it to extended size. Created in gimp.

### wtf2_extended_alt2

Same map as "wtf2_extended", but with some adjustments and changed colors. Created in gimp.

### emo

A map ported from gusanos promode mod, with some adjustments made by myself and changed colors. Created in gimp. Original map made probably by wesz and/or guth.

### emo_extended

Same map as "emo", but changed a bit to adjust it to extended size. Created in gimp.

### emo_small

Same map as "emo" actually, but changed a little bit (resized to standard 504x350 + reverted colours in rock obstacles). Created in gimp.

### JDM

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp. Original map made by Jammang.

### koala

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp. Original map made by ZoMbIe828.

### val

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp. Original map made by ulv.

### dn_core

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp.

### dn_core_alt

Same map as "dn_core", but changed a bit to adjust it to [Webliero Extended](https://www.vgm-quiz.com/dev/webliero/extended) game modes. Created in gimp.

### hewy

A map ported from gusanos promode mod, with some adjustments made by myself and changed colors. A famous Salvador Dali's paining "The Persistence of Memory" was used as a background. Original map made probably by wesz and/or guth.

### nerd

A map ported from gusanos promode mod, with some adjustments made by myself and changed colors. The picture used in the background is a screenshot taken from classic oldschool game "Prince of Persia" (1989). Original map made probably by wesz and/or guth.

### leek

A map ported from gusanos promode mod, with some adjustments made by myself and changed colors. The picture used in the background is a "Star Wars" fan art. Original map made probably by wesz and/or guth.

### gew

A map ported from gusanos promode mod, with some adjustments made by myself and changed colors. A famous Edvard Munch's painting "The Scream" was used as a background. Original map made probably by wesz and/or guth.
