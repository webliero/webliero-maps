## Jetmen Revival Maps

### MountDoom

A map ported from old classic game Jetmen Revival 1.2 with some adjustments made by myself. It's very big already so I reccomend to play it in normal (non-mirrored) mode (although u can also play it in extended mode). Created in gimp. Uploaded in 3 versions: normal (big), medium and small (even the small one is much bigger than standard 504x350 map size).

### CaveDot

Another map ported from old classic game Jetmen Revival 1.2 with some adjustments made by myself. Created in gimp.

### CaveDot (extended)

Same map as "CaveDot", but changed a bit to adjust it to extended mode. Created in gimp.

### LunarDig

Another map ported from old classic game Jetmen Revival 1.2 with some adjustments made by myself. It's very big already so I reccomend to play it in normal (non-mirrored) mode (although u can also play it in extended mode). Created in gimp. Uploaded in 3 versions: normal (big), medium and small (even the small one is much bigger than standard 504x350 map size).

### PostNuke

Another map ported from old classic game Jetmen Revival 1.2 with some adjustments made by myself. It's very big already so I reccomend to play it in normal (non-mirrored) mode (although u can also play it in extended mode). Created in gimp. Uploaded in 3 versions: normal (big), medium and small (even the small one is much bigger than standard 504x350 map size).

### Garden

Another map ported from old classic game Jetmen Revival 1.2 with some adjustments made by myself. It's very big already so I reccomend to play it in normal (non-mirrored) mode (although u can also play it in extended mode). Created in gimp. Uploaded in 3 versions: normal (big), medium and small (even the small one is much bigger than standard 504x350 map size).
