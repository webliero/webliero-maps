## REMIXES

In this subdirectory you can find my own remixes of some Liero/OpenLieroX/WebLiero maps. Enjoy!

### Circle-k

A remix of Krymzon's Liero map **Circling**. Created in gimp.

### CUNT

A remix of etc's Liero map **COUNT**. Created in gimp.

### not_that_simple

A remix of a famous Liero map **simple** (based on Patrys' version). Created in gimp.

### not_simple_at_all

Another remix of **simple** (Patrys' version again). Created in gimp.

### tune-l_playable_edition

A remix of Fisch_PL's Liero map **tune-l**. Created in gimp.

### bunnydog

A remix of Greybrow's famous Liga Liero map **bunnycat**. Created in gimp.

### teraz3_fixed

A remix of another Liga Liero map **teraz3** made by sir_Kris. Created in gimp.

### arenaofroock

A remix of FireAarro's Liero map **Arena of Stone**. Created in gimp.

### closejump

A remix of a Liero Hell Hole map **openjump** (author unknown). Created in gimp.

### roo3arena

A remix of Gary Lloyd's Liero map **Q3arena1**. Created in gimp.

### manson4e

A remix of Biernath_John's map **manson**, intended to play with some WebLiero Extended hacks only. Created in gimp.

### WebLieroRacing

A remix of Judge's OLX map **LieroRacing**, intended to play with some WebLiero Extended hacks only. Created in gimp.

### WebLieroRacing_alt

Another remix of Judge's OLX map **LieroRacing**. Created in gimp.

### dirt_platforms

A remix of dsds's WebLiero map **dirts_2**, intended to play with some WebLiero Extended hacks only (especially in "dirts" mode). Created in gimp.

### dirt_platforms_alt

Another remix of dsds's WebLiero map **dirts_2**, but with some small tweaks. Created in gimp.

**_roo_**