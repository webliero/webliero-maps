## (kanga)roo maps

### ARENA

My own idea for a map. One of the first maps I have ever made for Liero. Created in levedit (sic!) and then converted into png format using gimp.

### ARENA_SW

My own idea for a map. One of the first maps I have ever made for Liero. Created in levedit (sic!) and then converted into png format using gimp.

### asshat_temple

Kinda "easter egg" map. It refers to the name of the first webliero "clan" called [ASS] and its ancestor. Created in WormHole2 and then converted into png format using gimp.

### dali+ffa

My inspiration for creating this map was one of the paintings by Salvador Dali. Very bizarre but original. Created in gimp.

### giger3

My inspiration for creating this map was one of the paintings by H.R. Giger, the creator of Alien. The map is creepy, but fun to play. Created in WormHole2 and then converted into png format using gimp.

### gonad2

My original idea for a map. Created in WormHole2 and then converted into png format using gimp.

### got4

Another "easter egg" map which refers to famous webliero player SuckMyGauss and his ultimate enemy - noname turd. Created in WormHole2 and then converted into png format using gimp.

### grumpy_cat

My own idea of a map. Despite the fact that the map is made in standard size (504x350), the fighting area (background) is much smaller, but this was done intentionally (the map is intended to be used for duels, forcing offensive games). Created in gimp.

### guernica2

My original idea for a map, but I was inspired by famous Pablo Picasso's painting called Guernica (which was used as a background). Created in WormHole2 and then converted into png format using gimp.

### hexagonal

My original idea for a map. Created in WormHole2 and gimp.

### vault_boy

My own idea of a map, however it was slightly inspired by 神風's map "despair". The map is kinda tricky, cuz the floor is made with undefined material - special rock (so that bullets pass through it), in order to prevent from spammy game. Created in WormHole2 and gimp.

### yennefer

My own idea for a map, but I used Yennefer's picture as a background. Created in WormHole2 and then converted into png format using gimp.
