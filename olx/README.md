# OpenLieroX Conversions

These maps were converted from the OpenLieroX [Ultimate Level Pack](https://www.openlierox.net/downloads/).
Several of the maps in this pack are themselves conversions or modifications of Gusanos maps.
The versions contained here are all direct conversions of the LXL versions.

## Conversion Set

The `conversion` directory contains powerlevel conversions of the OLX collection.
These are compatible with WebLiero rooms and clients, as well as extended.

### PowerLevel Palette

Each map has an embedded palette to reproduce the source level as accurately as possible.
WebLiero does not natively support loading PowerLevel palettes, however they can be supported by injecting the map palette into the mod's WLSPRT via the room script.
Even without the powerlevel palette, the maps will still load and be playable with any mod, albeit without the custom colors.

These powerlevels only use custom colors for the following ranges:
- **Rock:** `19-29`
- **Dirt:** `12-18,176-180`
- **Background:** `160-167`

## Extended Conversion Set

The `ext` directory contains powerlevel2 conversions of the OLX collection.
These are only compatible with extended WebLiero rooms and clients.

### PowerLevel Palette

The extended conversion set maps also rely on powerlevel palettes, however they additionally require the material hack to alter the material mapping, which is also only possible via room scripting.

These powerlevels only use custom colors for the following ranges:
- **Rock:** `224-255`
- **Dirt:** `192-223`
- **Background:** `3-11,39-47,120-125,160-167`

Note: The material mapping should be updated correspondingly.

## Note: BG Hack (Extended)

Both conversion sets contain maps consisting of a pair of images, e.g. `LEVEL.png` and `LEVEL-background.png`. These can be used in tandem with extended to support more graceful destruction of dirt. Alternatively, you may choose to load only the background version of the map, which contains no diggable materials.

